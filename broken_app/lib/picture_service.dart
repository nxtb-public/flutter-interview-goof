import 'dart:convert';

import 'package:http/http.dart' as http;

/// Provides pictures to the app.
class PictureService {
  /// Fetches a set of photos from the Picsum API.
  ///
  /// The Picsum API is a simple HTTP API that serves up images.
  Future<List<String>> get latest async {
    return http
        .get(Uri.parse('https://picsum.photos/v2/list'))
        .then((response) {
      final images = jsonDecode(response.body) as List<dynamic>;

      return images.map((image) => image['download_url'] as String).toList();
    });
  }
}
