import 'package:flutter/services.dart';

class Login_Service {
  final channel = MethodChannel('bapp');
  Future<bool> login(String email, String password) async {
    return (await channel.invokeMethod<bool>('login', {
      'email': email,
      'password': password,
    }))!;
  }
}
