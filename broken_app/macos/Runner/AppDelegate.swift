import Cocoa
import FlutterMacOS

@NSApplicationMain
class AppDelegate: FlutterAppDelegate {
  override func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
    return true
  }
  
  override func applicationDidFinishLaunching(_ notification: Notification) {
    let vc = mainFlutterWindow.contentViewController as! FlutterViewController
    FlutterMethodChannel(name: "bapp", binaryMessenger: vc.engine.binaryMessenger).setMethodCallHandler { call, result in
      if call.method == "login" {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
          result(true)
        }
      } else {
        result(FlutterMethodNotImplemented)
      }
    }
  }
}
