import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
      
    if let flutterView = window?.rootViewController as? FlutterViewController, let engine = flutterView.engine {
      FlutterMethodChannel(name: "bapp", binaryMessenger: engine.binaryMessenger).setMethodCallHandler { call, result in
        if call.method == "login" {
          DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            result(true)
          }
        } else {
          result(FlutterMethodNotImplemented)
        }
      }
    }
      
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
