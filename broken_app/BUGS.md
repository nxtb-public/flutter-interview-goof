1. On small screens, some error messages appear regarding the layout
1. When focussing the password field on a smaller screen, the input is not visible
1. The password is visible to the user, it should be obscured
1. When users press the login button without entering data, there seems to be a error in the console. 
   What are good strategies to prevent this case?
1. Customers request pressing next on the email field should go to the password field
1. The pictures-screen shows a back button, but should not allow the user to go back
1. The pictures are not cached, so every time the user scrolls up and down, the images reload
1. The project contains a lot of style problems, look for naming, 
   code formatting and visibility problems.
1. The pictures screen does not load images when the network connection is unavailable
   What are good methods to recover?
1. There is a widget test, but it does not function currently. 
   Which architecture decisions need to be made?