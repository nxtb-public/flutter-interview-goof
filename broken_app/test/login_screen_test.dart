import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:broken_app/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(const MyApp());

    expect(find.text('eMail'), findsOneWidget);
    expect(find.text('Password'), findsOneWidget);

    await tester.enterText(find.byKey(ValueKey('email')), 'test@test.com');
    await tester.enterText(find.byKey(ValueKey('password')), '12341234');

    await tester.tap(find.byType(ElevatedButton));

    // How can we verify the `UserService` received the correct data? 
  });
}
