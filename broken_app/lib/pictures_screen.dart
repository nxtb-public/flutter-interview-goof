import 'package:broken_app/picture_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PicturesScreen extends StatelessWidget {
  const PicturesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pictures'),
      ),
      body: FutureBuilder<List<String>>(
        future: PictureService().latest,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          }

          final pictures = snapshot.data!;
          return GridView.builder(
            itemCount: pictures.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
            ),
            itemBuilder: (context, index) {
              return Image.network(
                pictures[index],
                fit: BoxFit.cover,
              );
            },
          );
        },
      ),
    );
  }
}
