package com.nextbase.broken_app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodChannel;

public class MainActivity extends FlutterActivity {

  private MethodChannel methodChannel;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    final BinaryMessenger messenger = getFlutterEngine().getDartExecutor().getBinaryMessenger();
    methodChannel = new MethodChannel(messenger, "bapp");
    methodChannel.setMethodCallHandler((call, result) -> {
      if ("login".equalsIgnoreCase(call.method)) {
        final Handler mainHandler = new Handler(Looper.getMainLooper());
        new Thread(() -> {
          try {
            Thread.sleep(3000);
          } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
          }

          mainHandler.post(() -> result.success(true));
        }).start();
      } else {
        result.notImplemented();
      }
    });

  }
}
