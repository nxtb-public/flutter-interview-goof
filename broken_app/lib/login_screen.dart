import 'package:broken_app/LoginService.dart';
import 'package:broken_app/pictures_screen.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();

  bool _loggingIn = false;
  String? _email;
  String? _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset('images/nextbase-logo.png'),
                const SizedBox(height: 24),
                Text(
                  'Welcome to BApp.',
                  style: Theme.of(context).textTheme.headline5,
                ),
                SizedBox(height: 24),
                Text(
                  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec neque elit, hendrerit eu auctor non, pretium et sapien. ',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(height: 24),
                TextFormField(
                  key: ValueKey('email'),
                  decoration: const InputDecoration(label: Text('eMail')),
                  keyboardType: TextInputType.emailAddress,
                  onSaved: (input) {
                    _email = input;
                  },
                ),
                const SizedBox(height: 16),
                TextFormField(
                  key: ValueKey('password'),
                  decoration: const InputDecoration(label: Text('Password')),
                  textInputAction: TextInputAction.done,
                  onSaved: (input) {
                    _password = input;
                  },
                ),
                const SizedBox(height: 24),
                Row(
                  children: [
                    Text(
                      'By Logging in, you accept the terms & conditions',
                    ),
                    const SizedBox(width: 8),
                    if (_loggingIn) ...[
                      const SizedBox(
                        child: CircularProgressIndicator(),
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 8),
                    ],
                    ElevatedButton(
                      onPressed: () async {
                        setState(() {
                          _loggingIn = true;
                        });
                        bool successful = await LOGIN();
                        setState(() {
                          _loggingIn = false;
                        });

                        if (successful) {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) {
                              return const PicturesScreen();
                            },
                          ));
                        }
                      },
                      child: const Text('Log In'),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> LOGIN() async {
    return await Login_Service().login(_email!, _password!);
  }
}
